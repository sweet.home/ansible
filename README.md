# Ansible commands
## Apply playbook
```bash
ansible-playbook -i hosts site.yml -k --user=cyrill --become --ask-become-pass
```
## Show available facts
```bash
ansible localhost -m setup -a 'gather_subset=!all,!any,facter'
ansible localhost -m setup 
```